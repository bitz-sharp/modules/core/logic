﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Bitz.Modules.Core.Foundation.Interfaces;
using Bitz.Modules.Core.Foundation.Logic;
using Bitz.Modules.Core.Foundation.Services;

// ReSharper disable once CheckNamespace
namespace Bitz.Modules.Core.Logic
{
    /// <summary>
    ///     The UpdateService is responsible for ensuring all IUpdateable objects have their UpdateFunction called within a
    ///     reasonable time frame of their expected update interval
    /// </summary>
    public class GameLogicService : Service, IGameLogicService
    {
        /// <summary>
        ///     A cached version of the updateables list which is used by the UpdateServiceWorkers, this is created from the
        ///     _IUpdateables list at the beginning of an update if the _IUpdateables list changes
        /// </summary>
        private readonly List<IUpdateable> _CachedUpdateables = new List<IUpdateable>();

        /// <summary>
        ///     Current list of all updateables registered with the update service
        /// </summary>
        private readonly List<IUpdateable> _IUpdateables = new List<IUpdateable>();

        private readonly Stopwatch _Stopwatch = Stopwatch.StartNew();
        private Single _AverageUpdateMS;

        /// <summary>
        ///     Specifies whether the cached updateables list needs refreshing
        /// </summary>
        private Boolean _CacheUpdateRequired = true;

        private TimeSpan _CurrentTickSize;
        private TimeSpan _LastUpdateInterval = TimeSpan.Zero;

        public GameLogicService() : base(true)
        {
        }

        public Single Ups { get; set; } = 60;

        public TimeSpan CurrentIntervalTime => _LastUpdateInterval;
        public TimeSpan CurrentTickSize => _CurrentTickSize;

        public void TriggerUpdate(TimeSpan currentInstanceTime)
        {
            Update(currentInstanceTime);
        }

        public override void Initialize()
        {
        }

        public override String GetServiceDebugStatus()
        {
            return $"U:{_IUpdateables.Count.ToString()},AUMS:{_AverageUpdateMS.ToString()}";
        }

        public void RegisterIUpdateable(IUpdateable updateable)
        {
            _IUpdateables.Add(updateable);
            _CacheUpdateRequired = true;
        }

        public void UnRegisterIUpdateable(IUpdateable updateable)
        {
            _IUpdateables.Remove(updateable);
            _CacheUpdateRequired = true;
        }

        protected override void Update(TimeSpan currentInstanceTime)
        {
            Double swCurrent = _Stopwatch.Elapsed.TotalMilliseconds;
            if ((_Stopwatch.Elapsed - _LastUpdateInterval).TotalSeconds > 1.0f / Ups)
            {
                if (_CacheUpdateRequired)
                {
                    _CachedUpdateables.Clear();
                    _CachedUpdateables.AddRange(_IUpdateables);
                    _CacheUpdateRequired = false;
                }

                _CurrentTickSize = TimeSpan.FromSeconds(1.0f / Ups);
                _LastUpdateInterval += _CurrentTickSize;
                foreach (IUpdateable updateable in _CachedUpdateables) updateable.TryUpdate(_LastUpdateInterval);
                _AverageUpdateMS = (Single) (_AverageUpdateMS * 49 + (_Stopwatch.Elapsed.TotalMilliseconds - swCurrent)) / 50.0f;
            }
        }

        /// <summary>
        ///     This method causes the GameLogicService to jump forward any time it may have gotten behind, Can be used to
        ///     stabilize the system after a blocking operation
        /// </summary>
        public void ForceCatchup()
        {
            _LastUpdateInterval = _Stopwatch.Elapsed;
        }

        protected override void Shutdown()
        {
            _IUpdateables.Clear();
            _CachedUpdateables.Clear();
        }
    }
}