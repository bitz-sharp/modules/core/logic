﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bitz.Modules.Core.Foundation.Logic;

namespace Bitz.Modules.Core.Logic
{
    public class LogicWeb<T1> : UpdateableObject
    {
        private readonly List<WebState> _States = new List<WebState>();
        private WebState _CurrentState;
        private T1 _TargetObject;

        public Action OnStateChange { get; set; }

        public void SetTargetObject(T1 targetObject)
        {
            _TargetObject = targetObject;
        }

        public void SetState(WebState newState)
        {
            _CurrentState?.Exit();
            _CurrentState = newState;
            _CurrentState.Enter();
            OnStateChange?.Invoke();
        }

        /// <inheritdoc />
        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
            Predicate nextPredicate = _CurrentState.PossiblePredicates.FirstOrDefault(a => a.TestCondition(_TargetObject));
            if (nextPredicate != null) SetState(nextPredicate.NextState);
        }

        public class WebState
        {
            private readonly Action<T1> _OnEnter;
            private readonly Action<T1> _OnExit;
            private readonly LogicWeb<T1> _Web;
            public List<Predicate> PossiblePredicates = new List<Predicate>();

            public WebState(LogicWeb<T1> web, Action<T1> onEnter = null, Action<T1> onExit = null)
            {
                _OnEnter = onEnter;
                _OnExit = onExit;
                _Web = web;
                _Web._States.Add(this);
            }

            public void Enter()
            {
                _OnEnter?.Invoke(_Web._TargetObject);
            }

            public void Exit()
            {
                _OnExit?.Invoke(_Web._TargetObject);
            }
        }

        public class Predicate
        {
            public WebState NextState { get; set; }
            public Func<T1, Boolean> TestCondition { get; set; }
        }
    }
}