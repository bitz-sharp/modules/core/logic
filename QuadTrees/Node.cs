﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bitz.Modules.Core.Foundation.Interfaces;
using OpenTK;

namespace Bitz.Modules.Core.Logic.QuadTrees
{
    public partial class QuadTree
    {
        private class Node
        {
            private readonly Int32 _Level;
            private Single _NodePosX;
            private Single _NodePosY;
            private QuadTree _ParentTree;

            public Node(QuadTree parentTree, Int32 level)
            {
                _ParentTree = parentTree;
                Objects = new List<IPositionable2D>();
                _Level = level;
            }

            private List<IPositionable2D> Objects { get; set; }

            private Node TlNode { get; set; }

            private Node TrNode { get; set; }

            private Node BlNode { get; set; }

            private Node BrNode { get; set; }

            internal Vector4 DescribeQuadTree(ref List<Tuple<Int32, Vector4>> data)
            {
                if (Objects != null && Objects.Count > 0)
                {
                    Single xMin = Objects.Select(a => a.Position.X).Min(a => a);
                    Single yMin = Objects.Select(a => a.Position.Y).Min(a => a);
                    Single xMax = Objects.Select(a => a.Position.X).Max(a => a);
                    Single yMax = Objects.Select(a => a.Position.Y).Max(a => a);
                    Vector4 mine = new Vector4(xMin, yMin, xMax, yMax);
                    data.Add(new Tuple<Int32, Vector4>(_Level, mine));
                    return mine;
                }
                else
                {
                    List<Vector4?> myVectors = new List<Vector4?>
                    {
                        TlNode?.DescribeQuadTree(ref data),
                        TrNode?.DescribeQuadTree(ref data),
                        BlNode?.DescribeQuadTree(ref data),
                        BrNode?.DescribeQuadTree(ref data)
                    };

                    IEnumerable<Vector4?> notNull = myVectors.Where(a => a != null);
                    if (!notNull.Any()) return Vector4.Zero;

                    Single xMin = notNull.Select(a => a.Value.X).Min(a => a);
                    Single yMin = notNull.Select(a => a.Value.Y).Min(a => a);
                    Single xMax = notNull.Select(a => a.Value.X).Max(a => a);
                    Single yMax = notNull.Select(a => a.Value.Y).Max(a => a);
                    Vector4 mine = new Vector4(xMin, yMin, xMax, yMax);
                    data.Add(new Tuple<Int32, Vector4>(_Level, mine));
                    return mine;
                }
            }

            public void GetAll(List<IPositionable2D> returnList)
            {
                returnList.AddRange(Objects);
                TlNode?.GetAll(returnList);
                TrNode?.GetAll(returnList);
                BlNode?.GetAll(returnList);
                BrNode?.GetAll(returnList);
            }

            public void Destroy()
            {
                Objects.Clear();
                TlNode?.Destroy();
                TrNode?.Destroy();
                BlNode?.Destroy();
                BrNode?.Destroy();
                _ParentTree = null;
            }

            public void Remove(IPositionable2D worldObject)
            {
                if (TlNode == null)
                {
                    Objects.Remove(worldObject);
                }
                else
                {
                    Vector2 pos = worldObject.Position;
                    if (pos.X < _NodePosX && pos.Y < _NodePosY)
                        TlNode.Remove(worldObject);
                    else if (pos.X >= _NodePosX && pos.Y < _NodePosY)
                        TrNode.Remove(worldObject);
                    else if (pos.X < _NodePosX && pos.Y >= _NodePosY)
                        BlNode.Remove(worldObject);
                    else if (pos.X >= _NodePosX && pos.Y >= _NodePosY) BrNode.Remove(worldObject);
                }
            }

            public void Add(IPositionable2D worldObject)
            {
                if (TlNode == null)
                {
                    Objects.Add(worldObject);
                    if (Objects.Count > _ParentTree.SplitLimit)
                    {
                        Single minX = Int32.MaxValue;
                        Single minY = Int32.MaxValue;
                        Single maxX = Int32.MinValue;
                        Single maxY = Int32.MinValue;

                        foreach (IPositionable2D o in Objects)
                        {
                            Vector2 pos = o.Position;
                            minX = Math.Min(minX, pos.X);
                            minY = Math.Min(minY, pos.Y);
                            maxX = Math.Max(maxX, pos.X);
                            maxY = Math.Max(maxY, pos.Y);
                        }

                        _NodePosX = minX + (maxX - minX) * 0.5f;
                        _NodePosY = minY + (maxY - minY) * 0.5f;

                        TlNode = new Node(_ParentTree, _Level + 1);
                        TrNode = new Node(_ParentTree, _Level + 1);
                        BlNode = new Node(_ParentTree, _Level + 1);
                        BrNode = new Node(_ParentTree, _Level + 1);

                        foreach (IPositionable2D o in Objects)
                        {
                            Vector2 pos = o.Position;
                            if (pos.X < _NodePosX && pos.Y < _NodePosY)
                                TlNode.Add(o);
                            else if (pos.X >= _NodePosX && pos.Y < _NodePosY)
                                TrNode.Add(o);
                            else if (pos.X < _NodePosX && pos.Y >= _NodePosY)
                                BlNode.Add(o);
                            else if (pos.X >= _NodePosX && pos.Y >= _NodePosY) BrNode.Add(o);
                        }

                        _ParentTree.BranchesCount++;
                        Objects.Clear();
                    }
                }
                else
                {
                    Vector2 pos = worldObject.Position;
                    if (pos.X < _NodePosX && pos.Y < _NodePosY)
                        TlNode.Add(worldObject);
                    else if (pos.X >= _NodePosX && pos.Y < _NodePosY)
                        TrNode.Add(worldObject);
                    else if (pos.X < _NodePosX && pos.Y >= _NodePosY)
                        BlNode.Add(worldObject);
                    else if (pos.X >= _NodePosX && pos.Y >= _NodePosY) BrNode.Add(worldObject);
                }
            }

            public void Search(List<IPositionable2D> results, Vector2 minBound, Vector2 maxBound)
            {
                if (TlNode != null)
                {
                    Boolean searchTL = true;
                    Boolean searchTR = true;
                    Boolean searchBL = true;
                    Boolean searchBR = true;

                    if (minBound.X >= _NodePosX)
                    {
                        searchTL = false;
                        searchBL = false;
                    }

                    if (minBound.Y >= _NodePosY)
                    {
                        searchTL = false;
                        searchTR = false;
                    }

                    if (maxBound.X < _NodePosX)
                    {
                        searchTR = false;
                        searchBR = false;
                    }

                    if (maxBound.Y < _NodePosY)
                    {
                        searchBR = false;
                        searchBL = false;
                    }

                    if (searchTL) TlNode.Search(results, minBound, maxBound);

                    if (searchTR) TrNode.Search(results, minBound, maxBound);

                    if (searchBL) BlNode.Search(results, minBound, maxBound);

                    if (searchBR) BrNode.Search(results, minBound, maxBound);
                }
                else
                {
                    results.AddRange(Objects.Where(worldObject => worldObject.Position.X >= minBound.X && worldObject.Position.X < maxBound.X).Where(worldObject => worldObject.Position.Y >= minBound.Y && worldObject.Position.Y < maxBound.Y));
                }
            }
        }
    }
}