﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bitz.Modules.Core.Foundation.Interfaces;
using OpenTK;

namespace Bitz.Modules.Core.Logic.QuadTrees
{
    public partial class QuadTree
    {
        private List<Tuple<Int32, Vector4>> _QuadTreeDescriptionCache;
        private Node _RootNode;

        public QuadTree(Int32 splitLimit = 150)
        {
            _RootNode = new Node(this, 0);
            SplitLimit = splitLimit;
        }

        public Int32 SplitLimit { get; private set; }

        public Int32 ObjectsCount { get; private set; }

        public Int32 BranchesCount { get; private set; }


        public List<Tuple<Int32, Vector4>> DescribeQuadTree()
        {
            if (_QuadTreeDescriptionCache != null) return _QuadTreeDescriptionCache;
            List<Tuple<Int32, Vector4>> returnList = new List<Tuple<Int32, Vector4>>();
            _RootNode?.DescribeQuadTree(ref returnList);
            return returnList;
        }

        public void AddObject(IPositionable2D worldObject)
        {
            ObjectsCount++;
            _RootNode.Add(worldObject);
            _QuadTreeDescriptionCache = null;
        }

        public void RemoveObject(IPositionable2D worldObject)
        {
            ObjectsCount--;
            _RootNode.Remove(worldObject);
            _QuadTreeDescriptionCache = null;
        }

        public void PosUpdate(IPositionable2D worldObject)
        {
            _RootNode.Remove(worldObject);
            _RootNode.Add(worldObject);
            _QuadTreeDescriptionCache = null;
        }

        public List<IPositionable2D> Search(Vector2 minBounds, Vector2 maxBounds)
        {
            List<IPositionable2D> returnList = new List<IPositionable2D>();
            _RootNode.Search(returnList, minBounds, maxBounds);
            return returnList;
        }

        public void Rebalance()
        {
            List<IPositionable2D> currentItems = new List<IPositionable2D>();
            _RootNode.GetAll(currentItems);
            _RootNode.Destroy();
            _RootNode = new Node(this, 0);

            Vector2 averageCenter = Vector2.Zero;
            foreach (IPositionable2D item in currentItems) averageCenter += item.Position;

            averageCenter /= currentItems.Count;

            foreach (IPositionable2D item in currentItems.OrderByDescending(a => (a.Position - averageCenter).LengthSquared)) _RootNode.Add(item);
            _QuadTreeDescriptionCache = null;
        }
    }
}