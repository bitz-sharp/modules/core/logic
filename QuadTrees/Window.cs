﻿using System;
using System.Collections.Generic;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Interfaces;
using Bitz.Modules.Core.Foundation.Logic;
using OpenTK;

namespace Bitz.Modules.Core.Logic.QuadTrees
{
    public class Window : IDisposable
    {
        private readonly List<IPositionable2D> _CachedData;
        private IGameLogicService _GameLogicService;
        private Vector2 _LastCachePosition;

        private TimeSpan _LastCacheUpdate;
        
        public Window(QuadTree viewedTree, Single windowSize, TimeSpan dirtyAfterTime, Single dirtyAfterDistance)
        {
            ViewedTree = viewedTree;
            WindowSize = windowSize;
            DirtyAfterTime = dirtyAfterTime;
            DirtyAfterDistanceSq = dirtyAfterDistance * dirtyAfterDistance;
            _GameLogicService = Injector.GetSingleton<IGameLogicService>();
            _CachedData = new List<IPositionable2D>();
        }

        public Single WindowSize { get; set; }

        public QuadTree ViewedTree { get; set; }

        public TimeSpan DirtyAfterTime { get; set; }

        public Single DirtyAfterDistanceSq { get; set; }


        public void Dispose()
        {
            _CachedData.Clear();
            _GameLogicService = null;
            ViewedTree = null;
        }

        /// <summary>
        ///     things happen
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public IReadOnlyList<IPositionable2D> GetWindow(Vector2 position)
        {
            TimeSpan engineCurrentTime = _GameLogicService.CurrentIntervalTime;

            if ((position - _LastCachePosition).LengthSquared > DirtyAfterDistanceSq || engineCurrentTime - _LastCacheUpdate > DirtyAfterTime)
            {
                _LastCacheUpdate = engineCurrentTime;
                _LastCachePosition = position;
                _CachedData.Clear();
                _CachedData.AddRange(ViewedTree.Search(position - Vector2.One * 0.5f * WindowSize, position + Vector2.One * 0.5f * WindowSize));
            }

            return _CachedData.AsReadOnly();
        }
    }
}