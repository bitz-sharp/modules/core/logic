﻿using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Services;

namespace Bitz.Modules.Core.Logic.StateSystem
{
    public interface IStateService : IInjectable, IService
    {
        void EnterState(State stateToStart, State prevState = null);
        void ExitState(State stateToExit);
    }
}