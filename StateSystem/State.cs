﻿using System;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Interfaces;
using Bitz.Modules.Core.Foundation.Logic;

namespace Bitz.Modules.Core.Logic.StateSystem
{
    /// <summary>
    ///     A state class that is used as part of the StateSystem. This state class must be derived for each of the
    ///     possible states that are required. State progression is processed when NextState is set to a non null
    ///     value. This state eill exit immediately and the NextState will be entered. This bbehaviour can be changed
    ///     by overriding the IsComplete function of this class
    /// </summary>
    public abstract class State : UpdateableObject, INameable
    {
        private readonly IGameLogicService _GameLogicService;
        private readonly IStateService _StateService;
        private TimeSpan _TimeStateEntered;

        /// <summary>
        ///     Creates an instance of the State class
        /// </summary>
        /// <param name="name">The name of the state for debugging (defaults to the class name)</param>
        protected State(String name = "") : base(true)
        {
            Name = name;
            if (String.IsNullOrEmpty(Name)) Name = GetType().Name;
            _StateService = Injector.GetSingleton<IStateService>();
            _GameLogicService = Injector.GetSingleton<IGameLogicService>();
        }

        public Boolean StateActive { get; set; }

        public State NextState { get; set; }

        public TimeSpan ActiveTime => !StateActive ? TimeSpan.Zero : Injector.GetSingleton<IGameLogicService>().CurrentIntervalTime - _TimeStateEntered;

        public String Name { get; set; }

        /// <summary>
        ///     Event that triggers when the state is entered passing the previous state that passed control to this state if
        ///     applicable
        /// </summary>
        private event Action<State> _OnEnter;

        /// <summary>
        ///     Event that triggers when the state is exited
        /// </summary>
        private event Action _OnExit;

        public event Action<State> OnEnter
        {
            add => _OnEnter += value;
            remove => _OnEnter -= value;
        }

        public event Action OnExit
        {
            add => _OnExit += value;
            remove => _OnExit -= value;
        }

        internal void Enter(State previousState)
        {
            RegisterWithUpdateService();
            StateActive = true;
            _TimeStateEntered = _GameLogicService.CurrentIntervalTime;
            _OnEnter?.Invoke(previousState);
        }

        internal void Exit()
        {
            _OnExit?.Invoke();
            StateActive = false;
            UnRegisterWithUpdateService();
        }

        public override Boolean TryUpdate(TimeSpan currentInstanceTime)
        {
            Boolean returnValue = base.TryUpdate(currentInstanceTime);
            if (IsComplete())
            {
                _StateService.ExitState(this);
                if (NextState != null) _StateService.EnterState(NextState, this);
            }

            return returnValue;
        }

        public virtual Boolean IsComplete()
        {
            return NextState != null;
        }
    }
}