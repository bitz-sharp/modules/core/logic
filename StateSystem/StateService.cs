﻿using System;
using System.Collections.Generic;
using System.Text;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Services;

namespace Bitz.Modules.Core.Logic.StateSystem
{
    public class StateService : Service, IStateService
    {
        private readonly List<State> _CurrentlyActiveStates = new List<State>();

        public override String GetServiceDebugStatus()
        {
            StringBuilder debugOutput = new StringBuilder("CS:");
            foreach (State state in _CurrentlyActiveStates) debugOutput.Append($"{state.Name},");
            return debugOutput.ToString();
        }

        public void EnterState(State stateToStart, State prevState = null)
        {
            _CurrentlyActiveStates.Add(stateToStart);
            if (DebugMode) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.VERBOSE, $"State Service : Entering State {stateToStart.Name}");
            stateToStart.Enter(prevState);
        }

        public void ExitState(State stateToExit)
        {
            _CurrentlyActiveStates.Remove(stateToExit);
            if (DebugMode) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.VERBOSE, $"State Service : Exiting State {stateToExit.Name}");
            stateToExit.Exit();
            stateToExit.Dispose();
        }

        /// <inheritdoc />
        public override void Initialize()
        {
        }

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
        }

        protected override void Shutdown()
        {
            if (DebugMode) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.VERBOSE, "State Service : Shutting Down");
            _CurrentlyActiveStates.Clear();
        }
    }
}