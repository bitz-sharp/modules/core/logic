﻿using System;

namespace Bitz.Modules.Core.Logic.Triggers
{
    public class CountdownTrigger : LogicTrigger
    {
        private TimeSpan _TimeRemaining;

        public CountdownTrigger(TimeSpan triggerAfter, Action<LogicTrigger> logicToTrigger) : base(logicToTrigger)
        {
            _TimeRemaining = triggerAfter;
        }

        public static void FAF(TimeSpan triggerAfter, Action<LogicTrigger> logicToTrigger)
        {
            // ReSharper disable once ObjectCreationAsStatement
            new CountdownTrigger(triggerAfter, logicToTrigger);
        }

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
            _TimeRemaining -= timeSinceLastUpdate;
            base.Update(timeSinceLastUpdate);
        }

        protected override Boolean TestTrigger()
        {
            return _TimeRemaining <= TimeSpan.Zero;
        }
    }
}