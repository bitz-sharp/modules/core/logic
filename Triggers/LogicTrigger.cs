﻿using System;
using Bitz.Modules.Core.Foundation.Logic;

namespace Bitz.Modules.Core.Logic.Triggers
{
    public abstract class LogicTrigger : UpdateableObject
    {
        protected Action<LogicTrigger> _LogicToTrigger;

        protected LogicTrigger(Action<LogicTrigger> logicToTrigger)
        {
            _LogicToTrigger = logicToTrigger;
        }

        public Boolean Triggered { get; private set; }

        protected abstract Boolean TestTrigger();

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
            if (IsDisposed() || Triggered) return;
            if (TestTrigger())
            {
                _LogicToTrigger?.Invoke(this);
                Triggered = true;
                Dispose();
            }
        }

        public override void Dispose()
        {
            _LogicToTrigger = null;
            base.Dispose();
        }
    }
}