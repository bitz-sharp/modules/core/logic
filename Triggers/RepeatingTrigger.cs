﻿using System;

namespace Bitz.Modules.Core.Logic.Triggers
{
    public class RepeatingTrigger : LogicTrigger
    {
        private readonly TimeSpan _TriggerTime;

        private TimeSpan _TimeRemaining;

        public RepeatingTrigger(TimeSpan triggerAfter, Action<LogicTrigger> logicToTrigger) : base(logicToTrigger)
        {
            _TriggerTime = triggerAfter;
            _TimeRemaining = triggerAfter;
        }

        public static void FAF(TimeSpan triggerAfter, Action<LogicTrigger> logicToTrigger)
        {
            // ReSharper disable once ObjectCreationAsStatement
            new RepeatingTrigger(triggerAfter, logicToTrigger);
        }

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
            _TimeRemaining -= timeSinceLastUpdate;
            if (IsDisposed()) return;
            if (TestTrigger())
            {
                _LogicToTrigger?.Invoke(this);
                _TimeRemaining = _TriggerTime;
            }
        }

        protected override Boolean TestTrigger()
        {
            return _TimeRemaining <= TimeSpan.Zero;
        }
    }
}