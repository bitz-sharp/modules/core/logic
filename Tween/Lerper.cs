﻿/*

Created by Jacob Albano https://bitbucket.org/jacobalbano/glide/commits/7cf90c369db2bdaefbace67cb6e01d9e41ea8ee7
Modified by Steven Batchelor-Manning

The MIT License (MIT)

Copyright (c) 2013 Jacob Albano

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

using System;

namespace Bitz.Modules.Core.Logic.Tween
{
    public abstract class Lerper
    {
        [Flags]
        public enum Behavior
        {
            NONE = 0,
            REFLECT = 1,
            ROTATION = 2,
            ROTATION_RADIANS = 4,
            ROTATION_DEGREES = 8,
            ROUND = 16
        }

        protected const Single DEG = 180f / (Single) Math.PI;
        protected const Single RAD = (Single) Math.PI / 180f;

        public abstract void Initialize(Object fromValue, Object toValue, Behavior behavior);
        public abstract Object Interpolate(Single t, Object currentValue, Behavior behavior);
    }
}