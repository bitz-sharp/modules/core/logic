﻿using System;
using OpenTK;

namespace Bitz.Modules.Core.Logic.Tween
{
    internal class Lerpers
    {
        public class TimespanLerper : Lerper
        {
            private TimeSpan _From, _To, _Range;

            public override void Initialize(Object fromValue, Object toValue, Behavior behavior)
            {
                _From = (TimeSpan) fromValue;
                _To = (TimeSpan) toValue;
                _Range = _To - _From;
            }

            public override Object Interpolate(Single t, Object currentValue, Behavior behavior)
            {
                return _From.Ticks + _Range.Ticks * t;
            }
        }

        public class Vector2Lerper : Lerper
        {
            private Vector2 _From, _To, _Range;

            public override void Initialize(Object fromValue, Object toValue, Behavior behavior)
            {
                _From = (Vector2) fromValue;
                _To = (Vector2) toValue;
                _Range = _To - _From;
            }

            public override Object Interpolate(Single t, Object currentValue, Behavior behavior)
            {
                Single x = _From.X + _Range.X * t;
                Single y = _From.Y + _Range.Y * t;

                if (behavior.HasFlag(Behavior.ROUND))
                {
                    x = (Single) Math.Round(x);
                    y = (Single) Math.Round(y);
                }

                Vector2 current = (Vector2) currentValue;
                if (_Range.X != 0) current.X = x;
                if (_Range.Y != 0) current.Y = y;
                return current;
            }
        }

        public class Vector3Lerper : Lerper
        {
            private Vector3 _From, _To, _Range;

            public override void Initialize(Object fromValue, Object toValue, Behavior behavior)
            {
                _From = (Vector3) fromValue;
                _To = (Vector3) toValue;
                _Range = _To - _From;
            }

            public override Object Interpolate(Single t, Object currentValue, Behavior behavior)
            {
                Single x = _From.X + _Range.X * t;
                Single y = _From.Y + _Range.Y * t;
                Single z = _From.Z + _Range.Z * t;

                if (behavior.HasFlag(Behavior.ROUND))
                {
                    x = (Single) Math.Round(x);
                    y = (Single) Math.Round(y);
                    z = (Single) Math.Round(z);
                }

                Vector3 current = (Vector3) currentValue;
                if (_Range.X != 0) current.X = x;
                if (_Range.Y != 0) current.Y = y;
                if (_Range.Z != 0) current.Z = z;
                return current;
            }
        }

        public class Vector4Lerper : Lerper
        {
            private Vector4 _From, _To, _Range;

            public override void Initialize(Object fromValue, Object toValue, Behavior behavior)
            {
                _From = (Vector4) fromValue;
                _To = (Vector4) toValue;
                _Range = _To - _From;
            }

            public override Object Interpolate(Single t, Object currentValue, Behavior behavior)
            {
                Single w = _From.W + _Range.W * t;
                Single x = _From.X + _Range.X * t;
                Single y = _From.Y + _Range.Y * t;
                Single z = _From.Z + _Range.Z * t;

                if (behavior.HasFlag(Behavior.ROUND))
                {
                    w = (Single) Math.Round(w);
                    x = (Single) Math.Round(x);
                    y = (Single) Math.Round(y);
                    z = (Single) Math.Round(z);
                }

                Vector4 current = (Vector4) currentValue;
                if (_Range.W != 0) current.W = w;
                if (_Range.X != 0) current.X = x;
                if (_Range.Y != 0) current.Y = y;
                if (_Range.Z != 0) current.Z = z;
                return current;
            }
        }
    }
}