/*

Created by Jacob Albano https://bitbucket.org/jacobalbano/glide/commits/7cf90c369db2bdaefbace67cb6e01d9e41ea8ee7
Modified by Steven Batchelor-Manning

The MIT License (MIT)

Copyright (c) 2013 Jacob Albano

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Bitz.Modules.Core.Logic.Tween
{
    public partial class Tween
    {
        [Flags]
        public enum RotationUnit
        {
            DEGREES,
            RADIANS
        }

        private readonly List<Object> _End;
        private readonly List<Lerper> _Lerpers;
        private readonly IRemoveTweens _Remover;
        private readonly List<Object> _Start;
        private readonly Dictionary<String, Int32> _VarHash;

        private readonly List<GlideInfo> _Vars;
        private Lerper.Behavior _Behavior;

        private Boolean _FirstUpdate;
        private Int32 _RepeatCount, _TimesRepeated;

        private Tween(Object target, Single duration, Single delay, IRemoveTweens parent)
        {
            Target = target;
            _Duration = duration;
            _Delay = delay;
            _Remover = parent;

            _FirstUpdate = true;

            _VarHash = new Dictionary<String, Int32>();
            _Vars = new List<GlideInfo>();
            _Lerpers = new List<Lerper>();
            _Start = new List<Object>();
            _End = new List<Object>();
            _Behavior = Lerper.Behavior.NONE;
        }

        /// <summary>
        ///     The time remaining before the tween ends or repeats.
        /// </summary>
        public Single TimeRemaining => _Duration - _Time;

        /// <summary>
        ///     A value between 0 and 1, where 0 means the tween has not been started and 1 means that it has completed.
        /// </summary>
        public Single Completion
        {
            get
            {
                Single c = _Time / _Duration;
                return c < 0 ? 0 : c > 1 ? 1 : c;
            }
        }

        /// <summary>
        ///     Whether the tween is currently looping.
        /// </summary>
        public Boolean Looping => _RepeatCount != 0;

        /// <summary>
        ///     The object this tween targets. Will be null if the tween represents a timer.
        /// </summary>
        public Object Target { get; }

        private void AddLerp(Lerper lerper, GlideInfo info, Object from, Object to)
        {
            _VarHash.Add(info.PropertyName, _Vars.Count);
            _Vars.Add(info);

            _Start.Add(from);
            _End.Add(to);

            _Lerpers.Add(lerper);
        }

        private void Update(Single elapsed)
        {
            if (_FirstUpdate)
            {
                _FirstUpdate = false;

                Int32 i = _Vars.Count;
                while (i-- > 0)
                    if (_Lerpers[i] != null)
                        _Lerpers[i].Initialize(_Start[i], _End[i], _Behavior);
            }
            else
            {
                if (Paused)
                    return;

                if (_Delay > 0)
                {
                    _Delay -= elapsed;
                    if (_Delay > 0)
                        return;
                }

                if (_Time == 0 && _TimesRepeated == 0)
                    _Begin?.Invoke();

                _Time += elapsed;
                Single setTimeTo = _Time;
                Single t = _Time / _Duration;
                Boolean doComplete = false;

                if (_Time >= _Duration)
                {
                    if (_RepeatCount != 0)
                    {
                        setTimeTo = 0;
                        _Delay = _RepeatDelay;
                        _TimesRepeated++;

                        if (_RepeatCount > 0)
                            --_RepeatCount;

                        if (_RepeatCount < 0)
                            doComplete = true;
                    }
                    else
                    {
                        _Time = _Duration;
                        t = 1;
                        _Remover.Remove(this);
                        doComplete = true;
                    }
                }

                if (_Ease != null)
                    t = _Ease(t);

                Int32 i = _Vars.Count;
                while (i-- > 0)
                    if (_Vars[i] != null)
                        _Vars[i].Value = _Lerpers[i].Interpolate(t, _Vars[i].Value, _Behavior);

                _Time = setTimeTo;

                //	If the timer is zero here, we just restarted.
                //	If reflect mode is on, flip start to end
                if (_Time == 0 && _Behavior.HasFlag(Lerper.Behavior.REFLECT)) Reverse();

                _Update?.Invoke();

                if (doComplete) _Complete?.Invoke();
            }
        }

        #region Callbacks

        private Func<Single, Single> _Ease;
        private Action _Begin, _Update, _Complete;

        #endregion

        #region Timing

        public Boolean Paused { get; private set; }
        private Single _Delay, _RepeatDelay;
        private readonly Single _Duration;

        private Single _Time;

        #endregion

        #region Behavior

        /// <summary>
        ///     Apply target values to a starting point before tweening.
        /// </summary>
        /// <param name="values">The values to apply, in an anonymous type ( new { prop1 = 100, prop2 = 0} ).</param>
        /// <returns>A reference to this.</returns>
        public Tween From(Object values)
        {
            PropertyInfo[] props = values.GetType().GetProperties();
            foreach (PropertyInfo property in props)
            {
                Object propValue = property.GetValue(values, null);

                if (_VarHash.TryGetValue(property.Name, out Int32 index)) _Start[index] = propValue;

                //	if we aren't tweening this value, just set it
                new GlideInfo(Target, property.Name) {Value = propValue};
            }

            return this;
        }

        /// <summary>
        ///     Set the easing function.
        /// </summary>
        /// <param name="ease">The Easer to use.</param>
        /// <returns>A reference to this.</returns>
        public Tween Ease(Func<Single, Single> ease)
        {
            _Ease = ease;
            return this;
        }

        /// <summary>
        ///     Set a function to call when the tween begins (useful when using delays). Can be called multiple times for compound
        ///     callbacks.
        /// </summary>
        /// <param name="callback">The function that will be called when the tween starts, after the delay.</param>
        /// <returns>A reference to this.</returns>
        public Tween OnBegin(Action callback)
        {
            if (_Begin == null) _Begin = callback;
            else _Begin += callback;
            return this;
        }

        /// <summary>
        ///     Set a function to call when the tween finishes. Can be called multiple times for compound callbacks.
        ///     If the tween repeats infinitely, this will be called each time; otherwise it will only run when the tween is
        ///     finished repeating.
        /// </summary>
        /// <param name="callback">The function that will be called on tween completion.</param>
        /// <returns>A reference to this.</returns>
        public Tween OnComplete(Action callback)
        {
            if (_Complete == null) _Complete = callback;
            else _Complete += callback;
            return this;
        }

        /// <summary>
        ///     Set a function to call as the tween updates. Can be called multiple times for compound callbacks.
        /// </summary>
        /// <param name="callback">The function to use.</param>
        /// <returns>A reference to this.</returns>
        public Tween OnUpdate(Action callback)
        {
            if (_Update == null) _Update = callback;
            else _Update += callback;
            return this;
        }

        /// <summary>
        ///     Enable repeating.
        /// </summary>
        /// <param name="times">Number of times to repeat. Leave blank or pass a negative number to repeat infinitely.</param>
        /// <returns>A reference to this.</returns>
        public Tween Repeat(Int32 times = -1)
        {
            _RepeatCount = times;
            return this;
        }

        /// <summary>
        ///     Set a delay for when the tween repeats.
        /// </summary>
        /// <param name="delay">How long to wait before repeating.</param>
        /// <returns>A reference to this.</returns>
        public Tween RepeatDelay(Single delay)
        {
            _RepeatDelay = delay;
            return this;
        }

        /// <summary>
        ///     Sets the tween to reverse every other time it repeats. Repeating must be enabled for this to have any effect.
        /// </summary>
        /// <returns>A reference to this.</returns>
        public Tween Reflect()
        {
            _Behavior |= Lerper.Behavior.REFLECT;
            return this;
        }

        /// <summary>
        ///     Swaps the start and end values of the tween.
        /// </summary>
        /// <returns>A reference to this.</returns>
        public Tween Reverse()
        {
            Int32 i = _Vars.Count;
            while (i-- > 0)
            {
                Object s = _Start[i];
                Object e = _End[i];

                //	Set start to end and end to start
                _Start[i] = e;
                _End[i] = s;

                _Lerpers[i].Initialize(e, s, _Behavior);
            }

            return this;
        }

        /// <summary>
        ///     Whether this tween handles rotation.
        /// </summary>
        /// <returns>A reference to this.</returns>
        public Tween Rotation(RotationUnit unit = RotationUnit.DEGREES)
        {
            _Behavior |= Lerper.Behavior.ROTATION;
            _Behavior |= unit == RotationUnit.DEGREES ? Lerper.Behavior.ROTATION_DEGREES : Lerper.Behavior.ROTATION_RADIANS;

            return this;
        }

        /// <summary>
        ///     Whether tweened values should be rounded to integer values.
        /// </summary>
        /// <returns>A reference to this.</returns>
        public Tween Round()
        {
            _Behavior |= Lerper.Behavior.ROUND;
            return this;
        }

        #endregion

        #region Control

        /// <summary>
        ///     Cancel tweening given properties.
        /// </summary>
        /// <param name="properties"></param>
        public void Cancel(params String[] properties)
        {
            Int32 canceled = 0;
            foreach (String t in properties)
            {
                if (!_VarHash.TryGetValue(t, out Int32 index))
                    continue;

                _VarHash.Remove(t);
                _Vars[index] = null;
                _Lerpers[index] = null;
                _Start[index] = null;
                _End[index] = null;

                canceled++;
            }

            if (canceled == _Vars.Count)
                Cancel();
        }

        /// <summary>
        ///     Remove tweens from the tweener without calling their complete functions.
        /// </summary>
        public void Cancel()
        {
            _Remover.Remove(this);
        }

        /// <summary>
        ///     Assign tweens their final value and remove them from the tweener.
        /// </summary>
        public void CancelAndComplete()
        {
            _Time = _Duration;
            _Update = null;
            _Remover.Remove(this);
        }

        /// <summary>
        ///     Set tweens to pause. They won't update and their delays won't tick down.
        /// </summary>
        public void Pause()
        {
            Paused = true;
        }

        /// <summary>
        ///     Toggle tweens' paused value.
        /// </summary>
        public void PauseToggle()
        {
            Paused = !Paused;
        }

        /// <summary>
        ///     Resumes tweens from a paused state.
        /// </summary>
        public void Resume()
        {
            Paused = false;
        }

        #endregion
    }
}