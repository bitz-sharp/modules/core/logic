﻿/*

Created by Jacob Albano https://bitbucket.org/jacobalbano/glide/commits/7cf90c369db2bdaefbace67cb6e01d9e41ea8ee7
Modified by Steven Batchelor-Manning

The MIT License (MIT)

Copyright (c) 2013 Jacob Albano

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Reflection;
using OpenTK;

namespace Bitz.Modules.Core.Logic.Tween
{
    public class Tweener : Tween.TweenerImpl
    {
    }

    public partial class Tween
    {
        private interface IRemoveTweens //	lol get it
        {
            void Remove(Tween t);
        }

        public class TweenerImpl : IRemoveTweens
        {
            private static readonly Dictionary<Type, ConstructorInfo> _RegisteredLerpers;
            private readonly List<Tween> _AllTweens;
            private readonly List<Tween> _ToAdd;
            private readonly List<Tween> _ToRemove;
            private readonly Dictionary<Object, List<Tween>> _Tweens;

            static TweenerImpl()
            {
                _RegisteredLerpers = new Dictionary<Type, ConstructorInfo>();
                Type[] numericTypes =
                {
                    typeof(Int16),
                    typeof(Int32),
                    typeof(Int64),
                    typeof(UInt16),
                    typeof(UInt32),
                    typeof(UInt64),
                    typeof(Single),
                    typeof(Double)
                };

                for (Int32 i = 0; i < numericTypes.Length; i++) SetLerper<NumericLerper>(numericTypes[i]);
                SetLerper<Lerpers.Vector2Lerper>(typeof(Vector2));
                SetLerper<Lerpers.Vector3Lerper>(typeof(Vector3));
                SetLerper<Lerpers.Vector4Lerper>(typeof(Vector4));
                SetLerper<Lerpers.TimespanLerper>(typeof(TimeSpan));
            }

            protected TweenerImpl()
            {
                _Tweens = new Dictionary<Object, List<Tween>>();
                _ToRemove = new List<Tween>();
                _ToAdd = new List<Tween>();
                _AllTweens = new List<Tween>();
            }

            void IRemoveTweens.Remove(Tween tween)
            {
                _ToRemove.Add(tween);
            }

            /// <summary>
            ///     Associate a Lerper class with a property type.
            /// </summary>
            /// <typeparam name="TLerper">The Lerper class to use for properties of the given type.</typeparam>
            /// <param name="propertyType">The type of the property to associate the given Lerper with.</param>
            public static void SetLerper<TLerper>(Type propertyType) where TLerper : Lerper, new()
            {
                SetLerper(typeof(TLerper), propertyType);
            }

            /// <summary>
            ///     Associate a Lerper type with a property type.
            /// </summary>
            /// <param name="lerperType">The type of the Lerper to use for properties of the given type.</param>
            /// <param name="propertyType">The type of the property to associate the given Lerper with.</param>
            public static void SetLerper(Type lerperType, Type propertyType)
            {
                _RegisteredLerpers[propertyType] = lerperType.GetConstructor(new Type[0]);
            }

            /// <summary>
            ///     <para>Tweens a set of properties on an object.</para>
            ///     <para>To tween instance properties/fields, pass the object.</para>
            ///     <para>To tween static properties/fields, pass the type of the object, using typeof(ObjectType) or object.GetType().</para>
            /// </summary>
            /// <param name="target">The object or type to tween.</param>
            /// <param name="values">The values to tween to, in an anonymous type ( new { prop1 = 100, prop2 = 0} ).</param>
            /// <param name="duration">Duration of the tween in seconds.</param>
            /// <param name="delay">Delay before the tween starts, in seconds.</param>
            /// <param name="overwrite">Whether pre-existing tweens should be overwritten if this tween involves the same properties.</param>
            /// <returns>The tween created, for setting properties on.</returns>
            public Tween Tween<T>(T target, Object values, Single duration, Single delay = 0, Boolean overwrite = true) where T : class
            {
                if (target == null)
                    throw new ArgumentNullException(nameof(target));

                //	Prevent tweening on structs if you cheat by casting target as Object
                Type targetType = target.GetType();
                if (targetType.IsValueType)
                    throw new Exception("Target of tween cannot be a struct!");

                Tween tween = new Tween(target, duration, delay, this);
                _ToAdd.Add(tween);

                if (values == null) // valid in case of manual timer
                    return tween;

                PropertyInfo[] props = values.GetType().GetProperties();
                foreach (PropertyInfo t in props)
                {
                    if (overwrite && _Tweens.TryGetValue(target, out List<Tween> library))
                        foreach (Tween t1 in library)
                            t1.Cancel(t.Name);

                    PropertyInfo property = t;
                    GlideInfo info = new GlideInfo(target, property.Name);
                    GlideInfo to = new GlideInfo(values, property.Name, false);
                    Lerper lerper = CreateLerper(info.PropertyType);

                    tween.AddLerp(lerper, info, info.Value, to.Value);
                }

                AddAndRemove();
                return tween;
            }

            /// <summary>
            ///     Starts a simple timer for setting up callback scheduling.
            /// </summary>
            /// <param name="duration">How long the timer will run for, in seconds.</param>
            /// <param name="delay">How long to wait before starting the timer, in seconds.</param>
            /// <returns>The tween created, for setting properties.</returns>
            public Tween Timer(Single duration, Single delay = 0)
            {
                Tween tween = new Tween(null, duration, delay, this);
                AddAndRemove();
                _ToAdd.Add(tween);
                return tween;
            }

            /// <summary>
            ///     Remove tweens from the tweener without calling their complete functions.
            /// </summary>
            public void Cancel()
            {
                _ToRemove.AddRange(_AllTweens);
            }

            /// <summary>
            ///     Assign tweens their final value and remove them from the tweener.
            /// </summary>
            public void CancelAndComplete()
            {
                foreach (Tween t in _AllTweens) t.CancelAndComplete();
            }

            /// <summary>
            ///     Set tweens to pause. They won't update and their delays won't tick down.
            /// </summary>
            public void Pause()
            {
                foreach (Tween tween in _AllTweens) tween.Pause();
            }

            /// <summary>
            ///     Toggle tweens' paused value.
            /// </summary>
            public void PauseToggle()
            {
                foreach (Tween tween in _AllTweens) tween.PauseToggle();
            }

            /// <summary>
            ///     Resumes tweens from a paused state.
            /// </summary>
            public void Resume()
            {
                foreach (Tween tween in _AllTweens) tween.Resume();
            }

            /// <summary>
            ///     Updates the tweener and all objects it contains.
            /// </summary>
            /// <param name="secondsElapsed">Seconds elapsed since last update.</param>
            public void Update(Single secondsElapsed)
            {
                foreach (Tween t in _AllTweens) t.Update(secondsElapsed);

                AddAndRemove();
            }

            private static Lerper CreateLerper(Type propertyType)
            {
                if (!_RegisteredLerpers.TryGetValue(propertyType, out ConstructorInfo lerper))
                    throw new Exception($"No Lerper found for type {propertyType.FullName}.");

                return (Lerper)lerper.Invoke(null);
            }

            private void AddAndRemove()
            {
                for (Int32 i = 0; i < _ToAdd.Count; ++i)
                {
                    Tween tween = _ToAdd[i];
                    _AllTweens.Add(tween);
                    if (tween.Target == null) continue; //	don't sort timers by target

                    if (!_Tweens.TryGetValue(tween.Target, out List<Tween> list))
                        _Tweens[tween.Target] = list = new List<Tween>();

                    list.Add(tween);
                }

                for (Int32 i = 0; i < _ToRemove.Count; ++i)
                {
                    Tween tween = _ToRemove[i];
                    _AllTweens.Remove(tween);
                    if (tween.Target == null) continue; // see above

                    if (_Tweens.TryGetValue(tween.Target, out List<Tween> list))
                    {
                        list.Remove(tween);
                        if (list.Count == 0) _Tweens.Remove(tween.Target);
                    }

                    _AllTweens.Remove(tween);
                }

                _ToAdd.Clear();
                _ToRemove.Clear();
            }

            private class NumericLerper : Lerper
            {
                private Single _From, _To, _Range;

                public override void Initialize(Object fromValue, Object toValue, Behavior behavior)
                {
                    _From = Convert.ToSingle(fromValue);
                    _To = Convert.ToSingle(toValue);
                    _Range = _To - _From;

                    if (!behavior.HasFlag(Behavior.ROTATION)) return;

                    Single angle = _From;
                    if (behavior.HasFlag(Behavior.ROTATION_RADIANS))
                        angle *= DEG;

                    if (angle < 0)
                        angle = 360 + angle;

                    Single r = angle + _Range;
                    Single d = r - angle;
                    Single a = Math.Abs(d);

                    if (a >= 180) _Range = (360 - a) * (d > 0 ? -1 : 1);
                    else _Range = d;
                }

                public override Object Interpolate(Single t, Object current, Behavior behavior)
                {
                    Single value = _From + _Range * t;
                    if (behavior.HasFlag(Behavior.ROTATION))
                    {
                        if (behavior.HasFlag(Behavior.ROTATION_RADIANS))
                            value *= DEG;

                        value %= 360.0f;

                        if (value < 0)
                            value += 360.0f;

                        if (behavior.HasFlag(Behavior.ROTATION_RADIANS))
                            value *= RAD;
                    }

                    if (behavior.HasFlag(Behavior.ROUND)) value = (Single)Math.Round(value);

                    Type type = current.GetType();
#if !WEB
                    return Convert.ChangeType(value, type);
#else
                    return value;
#endif
                }
            }

            #region Target control

            /// <summary>
            ///     Cancel all tweens with the given target.
            /// </summary>
            /// <param name="target">The object being tweened that you want to cancel.</param>
            public void TargetCancel(Object target)
            {
                if (!_Tweens.TryGetValue(target, out List<Tween> list)) return;
                foreach (Tween t in list) t.Cancel();
            }

            /// <summary>
            ///     Cancel tweening named properties on the given target.
            /// </summary>
            /// <param name="target">The object being tweened that you want to cancel properties on.</param>
            /// <param name="properties">The properties to cancel.</param>
            public void TargetCancel(Object target, params String[] properties)
            {
                if (!_Tweens.TryGetValue(target, out List<Tween> list)) return;
                foreach (Tween t in list) t.Cancel(properties);
            }

            /// <summary>
            ///     Cancel, complete, and call complete callbacks for all tweens with the given target..
            /// </summary>
            /// <param name="target">The object being tweened that you want to cancel and complete.</param>
            public void TargetCancelAndComplete(Object target)
            {
                if (!_Tweens.TryGetValue(target, out List<Tween> list)) return;
                foreach (Tween t in list) t.CancelAndComplete();
            }


            /// <summary>
            ///     Pause all tweens with the given target.
            /// </summary>
            /// <param name="target">The object being tweened that you want to pause.</param>
            public void TargetPause(Object target)
            {
                if (!_Tweens.TryGetValue(target, out List<Tween> list)) return;
                foreach (Tween t in list) t.Pause();
            }

            /// <summary>
            ///     Toggle the pause state of all tweens with the given target.
            /// </summary>
            /// <param name="target">The object being tweened that you want to toggle pause.</param>
            public void TargetPauseToggle(Object target)
            {
                if (!_Tweens.TryGetValue(target, out List<Tween> list)) return;
                foreach (Tween t in list) t.PauseToggle();
            }

            /// <summary>
            ///     Resume all tweens with the given target.
            /// </summary>
            /// <param name="target">The object being tweened that you want to resume.</param>
            public void TargetResume(Object target)
            {
                if (!_Tweens.TryGetValue(target, out List<Tween> list)) return;
                foreach (Tween t in list) t.Resume();
            }

            #endregion
        }
    }
}